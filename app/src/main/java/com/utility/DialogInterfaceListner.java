package com.utility;

/**
 * <h>DialogInterfaceListner</h>
 * Created by Ali on 8/10/2017.
 */

public interface DialogInterfaceListner
{
    void dialogClick(boolean isClicked);
}
