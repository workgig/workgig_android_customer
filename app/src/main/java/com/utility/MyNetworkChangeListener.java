package com.utility;

/**
 * <h>MyNetworkChangeListener</h>
 * Created by Ali on 2/22/18.
 */
public interface MyNetworkChangeListener
{
     void onNetworkStateChanges(boolean nwStatus);
}
