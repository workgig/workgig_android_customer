package com.pojo;

/**
 * Created by ${3Embed} on 9/10/17.
 */

public class ResponsePojo {
    String name="Sara Francis";
    String service="Painter";
    String priceperhr="Quoted price $ 260 Per hr";
    String number="8547197794";

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getService() {
        return service;
    }

    public String getPriceperhr() {
        return priceperhr;
    }
}
