package com.workgig.providerdetails.viewschedule;

import com.workgig.home.BasePresenter;
import com.workgig.home.BaseView;
import com.pojo.ScheduleMonthPojo;

/**
 * <h>ProviderDetailsContract</h>
 * Created by Ali on 2/5/2018.
 */

public interface ScheduleContract
{
    interface SchedulePresenter extends BasePresenter
    {
       void getSchedule(String sessionToken, String date, boolean isCurrentMonth,String providerId);
       void onSuccessGetSchedule(String result);
    }
    interface ScheduleView extends BaseView
    {
        void onLogout(String message);
       void showProgress();
       void hideProgress();
        void onSuccessGetSchedule(ScheduleMonthPojo scheduleMonthPojo);

    }
}
