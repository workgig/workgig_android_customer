package com.workgig.videocalling;

import com.workgig.networking.ChatApiService;

public interface VideoCallContract {
    interface Presenter{
        void initCall();
        void endCall(String callID, String callFrom, ChatApiService chatApiService);
        void dropView();
        void dispose();
    }

    interface View{
        void onAnswerSuccess();
        void onRejectSuccess();
    }
}
