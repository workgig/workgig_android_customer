package com.workgig.videocalling;

import android.util.Log;

import com.workgig.networking.ChatApiService;
import com.workgig.networking.LSPServices;
import com.workgig.utilities.Constants;
import com.workgig.utilities.SessionManagerImpl;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class VideoCallPresenter implements VideoCallContract.Presenter {

    private String TAG = VideoCallPresenter.class.getSimpleName();

    @Inject
    VideoCallContract.View view;
    @Inject
    LSPServices apiServices;
    @Inject
    SessionManagerImpl sessionManager;
    private CompositeDisposable compositeDisposable;

    @Inject
    VideoCallPresenter(){
        compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void initCall() {

    }

    @Override
    public void endCall(String callID, String callFrom, ChatApiService chatApiService) {
        Log.d(TAG, "onNext: endCallResponse: "+sessionManager.getCallToken());

        chatApiService.endCall(sessionManager.getCallToken(), Constants.selLang,callID,callFrom)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {
                        try {
                            String response = value.body().string();
                            Log.d(TAG, "onNext: endCallResponse: "+response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if(view != null)
                            view.onRejectSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public void dispose() {
        compositeDisposable.clear();
    }


}
