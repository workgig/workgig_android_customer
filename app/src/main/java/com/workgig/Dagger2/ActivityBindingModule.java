package com.workgig.Dagger2;


import com.workgig.IntroActivity.IntroActivity;
import com.workgig.IntroActivity.IntroModule;
import com.workgig.Login.LoginActivity;
import com.workgig.Login.LoginModule;
import com.workgig.SplashScreen.SplashActivity;
import com.workgig.SplashScreen.SplashModule;
import com.workgig.addTocart.AddToCart;
import com.workgig.addTocart.AddToCartModule;
import com.workgig.add_address.AddAddressActivity;
import com.workgig.add_address.AddAddressModule;
import com.workgig.biddingFlow.BiddingModule;
import com.workgig.biddingFlow.BiddingQuestions;
import com.workgig.bookingtype.BookingType;
import com.workgig.bookingtype.BookingTypeDaggerModule;
import com.workgig.cancelledBooking.CancelledBooking;
import com.workgig.cancelledBooking.CancelledBookingModule;
import com.workgig.change_email.ChangeEmailActivity;
import com.workgig.change_email.ChangeEmailModule;
import com.workgig.changepassword.ChangePwdActivity;
import com.workgig.changepassword.ChangePwdModule;
import com.workgig.chatting.ChattingActivity;
import com.workgig.chatting.ChattingModule;
import com.workgig.confirmbookactivity.ConfirmBookActivity;
import com.workgig.confirmbookactivity.ConfirmBookingModule;
import com.workgig.faq.FaqActivity;
import com.workgig.faq.FaqModule;
import com.workgig.favouriteProvider.FavouriteProvider;
import com.workgig.favouriteProvider.FavouriteProviderModule;
import com.workgig.filter.FilterActivity;
import com.workgig.filter.FilterDaggerModule;
import com.workgig.filter.FilterDaggerResponseModule;
import com.workgig.forgotpassword.ForgotPwdActivity;
import com.workgig.forgotpassword.ForgotPwdModule;
import com.workgig.home.MainActivity;
import com.workgig.home.MainActivityDaggerModule;
import com.workgig.inCallOutCall.TimeSlots;
import com.workgig.inCallOutCall.TimeSlotsModule;
import com.workgig.invoice.InvoiceActivity;
import com.workgig.invoice.InvoiceDaggerModule;
import com.workgig.jobDetailsStatus.JobDetailsActivity;
import com.workgig.jobDetailsStatus.JobDetailsModules;
import com.workgig.otp.OtpActivity;
import com.workgig.otp.OtpModule;
import com.workgig.payment_details.PaymentDetailActivity;
import com.workgig.payment_details.PaymentDetailsModule;
import com.workgig.payment_edit_card.CardEditActivity;
import com.workgig.payment_edit_card.CardEditModule;
import com.workgig.payment_method.PaymentMethodActivity;
import com.workgig.payment_method.PaymentMethodModule;
import com.workgig.profile.ProfileActivity;
import com.workgig.profile.ProfileModule;
import com.workgig.promocode.PromoCodeActivity;
import com.workgig.promocode.PromoCodeDaggerModule;
import com.workgig.providerList.ProviderList;
import com.workgig.providerList.ProviderListDaggerModule;
import com.workgig.providerdetails.ProviderDetails;
import com.workgig.providerdetails.ProviderDetailsModules;
import com.workgig.providerdetails.viewschedule.ScheduleActivity;
import com.workgig.providerdetails.viewschedule.ScheduleModules;
import com.workgig.rateYourBooking.RateYourBooking;
import com.workgig.rateYourBooking.RateYourBookingModule;
import com.workgig.selectPaymentMethod.SelectDaggerModule;
import com.workgig.selectPaymentMethod.SelectPayment;
import com.workgig.share.ShareActivity;
import com.workgig.share.ShareScreenModule;
import com.workgig.signup.SignUpActivity;
import com.workgig.signup.SignUpModule;
import com.workgig.videocalling.ExitActivity;
import com.workgig.videocalling.IncomingCallDaggerModule;
import com.workgig.videocalling.IncomingCallScreen;

import com.workgig.wallet.WalletActivity;
import com.workgig.wallet.WalletActivityDaggerModule;
import com.workgig.walletTransaction.WalletTransActivity;
import com.workgig.walletTransaction.WalletTransactionActivityDaggerModule;
import com.workgig.youraddress.YourAddressActivity;
import com.workgig.youraddress.YourAddressModule;
import com.workgig.zendesk.zendeskHelpIndex.ZendeskAdapterModule;
import com.workgig.zendesk.zendeskHelpIndex.ZendeskHelpIndex;
import com.workgig.zendesk.zendeskHelpIndex.ZendeskModule;
import com.workgig.zendesk.zendeskTicketDetails.HelpIndexAdapterModule;
import com.workgig.zendesk.zendeskTicketDetails.HelpIndexTicketDetails;
import com.workgig.zendesk.zendeskTicketDetails.HelpTicketDetailsModule;
import com.utility.OnMyService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * @author Pramod
 * @since 11/12/17.
 */

@Module
public interface ActivityBindingModule
{
    @ContributesAndroidInjector
    OnMyService service();

    @ActivityScoped
    @ContributesAndroidInjector
    ExitActivity exitActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = IntroModule.class)
    IntroActivity provideIntroActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = LoginModule.class)
     LoginActivity loginActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = FaqModule.class)
    FaqActivity provideFaqActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = SplashModule.class)
     SplashActivity splashScreen();

    @ActivityScoped
    @ContributesAndroidInjector(modules = SignUpModule.class)
     SignUpActivity signUpActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ForgotPwdModule.class)
     ForgotPwdActivity forgotPwdActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = OtpModule.class)
     OtpActivity otpActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ChangePwdModule.class)
     ChangePwdActivity changePwdActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = MainActivityDaggerModule.class)
     MainActivity provideMainActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = BookingTypeDaggerModule.class)
    BookingType provideBookingType();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ProviderListDaggerModule.class)
    ProviderList provideProviderList();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {FilterDaggerModule.class, FilterDaggerResponseModule.class})
    FilterActivity provideFilterActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ProviderDetailsModules.class)
    ProviderDetails provideProviderDetails();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ScheduleModules.class)
    ScheduleActivity scheduleFragment();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ConfirmBookingModule.class)
    ConfirmBookActivity provideConfirmBookingActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = AddToCartModule.class)
    AddToCart provideAddToCart();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ProfileModule.class)
     ProfileActivity profileActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = YourAddressModule.class)
     YourAddressActivity yourAddressActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = AddAddressModule.class)
     AddAddressActivity addAddressActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = CardEditModule.class)
    CardEditActivity provideCardActivity();
    @ActivityScoped
    @ContributesAndroidInjector(modules = PaymentDetailsModule.class)
    PaymentDetailActivity providePaymentDetailsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = PaymentMethodModule.class)
    PaymentMethodActivity providePaymentMethodActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = RateYourBookingModule.class)
    RateYourBooking provideRateYourBooking();

    @ActivityScoped
    @ContributesAndroidInjector(modules = JobDetailsModules.class)
    JobDetailsActivity provideJobDetails();


    @ActivityScoped
    @ContributesAndroidInjector(modules = {ZendeskModule.class, ZendeskAdapterModule.class})
    ZendeskHelpIndex provideZendeskHelp();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {HelpTicketDetailsModule.class, HelpIndexAdapterModule.class})
    HelpIndexTicketDetails provideHelpIndexDetails();

    @ActivityScoped
    @ContributesAndroidInjector(modules = SelectDaggerModule.class)
    SelectPayment provideSelectPayment();

    @ActivityScoped
    @ContributesAndroidInjector(modules = CancelledBookingModule.class)
    CancelledBooking provideCancelledBooking();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ChangeEmailModule.class)
    ChangeEmailActivity provideChangeEmail();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ChattingModule.class)
    ChattingActivity provideChatting();

    @ActivityScoped
    @ContributesAndroidInjector(modules = WalletActivityDaggerModule.class)
    WalletActivity provideWalletActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = WalletTransactionActivityDaggerModule.class)
    WalletTransActivity provideWalletTransaction();

    @ActivityScoped
    @ContributesAndroidInjector(modules = PromoCodeDaggerModule.class)
    PromoCodeActivity providePromoCode();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ShareScreenModule.class)
    ShareActivity provideShareScree();

     @ActivityScoped
    @ContributesAndroidInjector(modules = BiddingModule.class)
     BiddingQuestions provideBiddingQuestion();

     @ActivityScoped
    @ContributesAndroidInjector(modules = FavouriteProviderModule.class)
     FavouriteProvider provideFavouritePro();



    @ActivityScoped
    @ContributesAndroidInjector(modules = IncomingCallDaggerModule.class)
    IncomingCallScreen provideIncomingCalling();

    @ActivityScoped
    @ContributesAndroidInjector(modules = TimeSlotsModule.class)
    TimeSlots provideTimeSlots();

    @ActivityScoped
    @ContributesAndroidInjector(modules = InvoiceDaggerModule.class)
    InvoiceActivity provideInvoiceActivity();


   /* @ActivityScoped
    @ContributesAndroidInjector(modules = ActivityAudioCallModule.class)
    AudioCallActivity provideAudioCall();*/




}
