package com.workgig.utilities;

public interface RemoveFavCallBack{
    public void onRemoveFav(String catId, String providerId );
}
