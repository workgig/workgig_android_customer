package com.workgig.utilities;

import android.os.Build;

import com.google.android.gms.maps.model.LatLng;
import com.workgig.BuildConfig;
import com.workgig.model.Offers;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * <h>Constants</h>
 * Created by ${3Embed} on 6/10/17.
 */

public class Constants {

    public static final String PARENT_FOLDER = "lsp";

    public static final int WALLETCALL = 15;
    public static boolean IS_CHATTING_OPENED = false;
    public static boolean IS_CHATTING_RESUMED = false;
    public static String currencySymbol = "$";
    public static String bookingcurrencySymbol = "$";
    public static final String PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;

    public static final String MQTT_URL_HOST = "tcp://165.22.7.96";
    public static final String MQTT_PORT = "2052";
    public static final String BASE_URL = "https://api.workgig.com";
    public static final String MQTT_TOPIC = "provider/";
    public static final String LOCATION_DATA_EXTRA = "LOCATION_DATA_EXTRA";

    public static final String CHAT_URL = "https://call.service-genie.xyz";

    public static final String CHAT_DATA_UPLOAD = "http://45.77.190.140:8009/";

    public static final String SERVER_KEY = "AIzaSyAbz187ZVtHiI6rK_bjtm1ILKa8gIqpMT8";
    public static final String Amazonbucket = "workgigbucket";
    public static final String Amazoncognitoid = "us-east-2:f6af7398-14f4-4816-b954-b3799684d9e4";

    public static final int GALLERY_PIC = 10;
    public static final int CAMERA_PIC = 11;
    public static final int CROP_IMAGE = 12;

    public static final Pattern PASSWORD_PATTERN = Pattern.compile("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{7,20}");

    public static final String APP_VERSION = BuildConfig.VERSION_NAME;
    public static final String DEVICE_MODEL = Build.MODEL;
    public static final String DEVICE_MAKER = Build.MANUFACTURER;
    public static final int DEVICE_TYPE = 2; //1-iOS , 2-android, 3-web
    public static final String DEVICE_OS_VERSION = Build.VERSION.RELEASE;

    public static String stripeKeys;

    public static double latitude = 0;
    public static double longitude = 0;

    public static double proLatitude = 0;
    public static double proLongitude = 0;
    public static String proAddress = "";
    public static double currentLat = 0;
    public static double currentLng = 0;
    public static boolean isFromNotification = false;


    public static LatLng custLatLng;

    public static int customerHomePageInterval = 5;
    public static String selLang = "en";

    public static String mqttErrorMsg = "";
    public static String providerId = "";

    public static double lat = 0.0;
    public static double lng = 0.0;
    public static double minPrice = 0.0;
    public static double maxPrice = 0.0;
    public static double distance = 30;

    /**
     * <h>bookingType</h>
     * 1 now booking
     * 2 late booking
     * 3 repeat
     */
    public static int bookingType = 1;

    /**
     * <H1>serviceType</H1>
     * service_type = 1 onDemand (ex Taxi booking) not price
     * service_type = 2 MarketPlace(ex Music) edit price
     * service_type = 3 Bidding
     */
    public static int serviceType = 0;

    /**
     * <H1>bookingModel</H1>
     * billing_model
     * 1 - fixed not edit
     * 2 - hourly + fixed not edit
     * 3 - hourly not edit
     * <p>
     * 4 - hourly edit
     * 5 - fixed edit
     * 6 - hourly + fixed edit
     * 7 - bidding
     */
    public static int bookingModel = 0;
    public static boolean isNeedApproveBycustomer;
    public static String catId = "";
    public static String catName = "";
    public static String subCatId = "";
    public static int bookingTypeNowSchedule = 0;
    public static int bookingOffer = 0;
    public static int offerType = 0;
    public static String offerName = "";
    public static ArrayList<Offers> offers = null;
    public static String proId = "0";
    public static int scheduledTime = 0;
    public static long onRepeatEnd = 0;
    public static String scheduledDate = "";
    public static int callType = 0; //1 in call 2 out call
    public static int bookingModelJobDetails; // 1 on-Demand 2 market place, 3 bidding
    public static ArrayList<String> onRepeatDays = new ArrayList<>();

    public static String distanceUnit = "";

    public static final int FILTER_RESULT_CODE = 6;
    public static final int ADDRESS_RESULT_CODE = 14;
    public static final int PAYMENT_RESULT_CODE = 151;
    public static final int REPEAT_RESULT_CODE = 152;
    public static final int AMOUNT_RESULT_CODE = 153;
    public static final int TIME_RESULT_CODE = 154;

    public static final int SUCCESS_RESPONSE = 200;
    public static final int SESSION_LOGOUT = 498;
    public static final int SESSION_EXPIRED = 440;
    public static final int SESSION_NoDues = 410;


    public static boolean isConfirmBook = false;

    public static String LiveTrackBookingPid = "";
    public static boolean IS_NETWORK_ERROR_SHOWED = false;
    public static boolean isHomeFragment = false;
    public static boolean isJobDetailsOpen = false;
    public static boolean isJobBidDetailsOpen = false;
    public static int jobType = 2;
    public static long fromTime = 0;
    public static long toTime = 0;
    public static boolean isMenuActivityCalled = false;

    public static String filteredAddress = "";
    public static double filteredLat = 0;
    public static double filteredLng = 0;
    public static double visitFee = 0;
    public static double pricePerHour = 0;
    public static int serviceSelected = 2;

    public static int paymentAbbr = 1;

    public static boolean isHelpIndexOpen = false;
    public static long serverTime = 0;
    public static long diffServerTime = 0;

    public static String walletCurrency = "$";
    public static double walletAmount = 0;
    public static boolean isLoggedIn = true;
    public static final String TERMS_LINK = "https://admin.workgig.com/supportText/customer/en_termsAndConditions.php";
    public static final String PRIVECY_LINK = "https://admin.workgig.com/supportText/customer/en_privacyPolicy.php";

    public static String selectedDate = "";
    public static String selectedDuration = "";


    /*******************************RepeatText Area **************************************/

    public static String repeatStartTime = "";
    public static String repeatStartDate = "";
    public static String repeatEndTime = "";
    public static String repeatEndDate = "";
    public static int repeatNumOfShift = 0;
    public static ArrayList<String> repeatDays = new ArrayList<>();
    public static ArrayList<String> GOOGLEKEY = new ArrayList<>();

    public static final String KILOMETER = "Kilometers";
    public static final String METER = "meters";
    public static final String NAUTICAL_MILES = "nauticalMiles";
    public static JSONArray jsonArray = new JSONArray();

    public static final String SIGNATURE_PIC_DIR = "Signature";
    public static final String SIGNATURE_UPLOAD = "SignatureUpload";
    public static final String AMAZON_BASE_URL = "https://s3.amazonaws.com/";
    public static final String JOB_COMPLETED_RAISE_INVOICE = "10";
    public static final String JOB_TIMER_INCOMPLETE = "15";
    public static int SUCCESS_RESULT = 5;
    public static int FAILURE_RESULT = -5;

}
