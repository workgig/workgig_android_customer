package com.workgig.zendesk.zendeskHelpIndex;

import com.workgig.home.BasePresenter;
import com.workgig.home.BaseView;
import com.workgig.zendesk.zendeskpojo.OpenClose;

import java.util.ArrayList;

/**
 * <h>ZendeskHelpIndexContract</h>
 * Created by Ali on 2/26/2018.
 */

public interface ZendeskHelpIndexContract
{
    interface Presenter extends BasePresenter
    {
        void onToGetZendeskTicket();
    }
    interface  ZendeskView extends BaseView
    {
        void onGetTicketSuccess();

        void onEmptyTicket();

        void onTicketStatus(OpenClose openClose, int openCloseSize, boolean isOpenClose);

        void onNotifyData(ArrayList<OpenClose> alOpenClose);
        void onRefreshing(boolean isRefreshing);
    }
}
