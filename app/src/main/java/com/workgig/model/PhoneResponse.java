package com.workgig.model;

/**
 * Created by Pramod on 14/12/17.
 */

public class PhoneResponse {
    private String email;

    public PhoneResponse(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
