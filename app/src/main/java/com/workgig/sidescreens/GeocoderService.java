package com.workgig.sidescreens;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.workgig.R;
import com.workgig.model.youraddress.YourAddrData;
import com.workgig.utilities.Constants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GeocoderService extends IntentService {

  private ResultReceiver resultReceiver;
  private static final String TAG = "GeocoderService";
  public GeocoderService() {
    super("GeocoderService");
  }

  @Override
  protected void onHandleIntent(@Nullable Intent intent) {

    if (intent == null) {
      return;
    }
    String errorMessage = "";
    Geocoder geocoder = new Geocoder(this);
    // Get the location passed to this service through an extra.
    Location location = intent.getParcelableExtra(
        Constants.LOCATION_DATA_EXTRA);
    resultReceiver = intent.getParcelableExtra("RECEIVER");
    List<Address> addresses = null;

    try {
      addresses = geocoder.getFromLocation(
          location.getLatitude(),
          location.getLongitude(),
          // In this sample, get just a single address.
          1);
    } catch (IOException ioException) {
      // Catch network or other I/O problems.
      errorMessage = getString(R.string.service_not_available);
      Log.e(TAG, errorMessage, ioException);
    } catch (IllegalArgumentException illegalArgumentException) {
      // Catch invalid latitude or longitude values.
      errorMessage = getString(R.string.invalid_lat_long_used);
      Log.e(TAG, errorMessage + ". " +
          "Latitude = " + location.getLatitude() +
          ", Longitude = " +
          location.getLongitude(), illegalArgumentException);
    }

    // Handle case where no address was found.
    if (addresses == null || addresses.size()  == 0) {
      if (errorMessage.isEmpty()) {
        errorMessage = getString(R.string.no_address_found);
        Log.e(TAG, errorMessage);
      }
      deliverResultToReceiver(Constants.FAILURE_RESULT , "","","");
    } else {
      Address address = addresses.get(0);
      Log.i(TAG, getString(R.string.address_found));
      deliverResultToReceiver(Constants.SUCCESS_RESULT,address.getLocality(),address.getAdminArea(),address.getCountryName());
    }

  }
  public void deliverResultToReceiver(int resultcode,String city, String state ,String country){
    if(resultReceiver!=null){
      Bundle bundle = new Bundle();
      bundle.putString("city",city);
      bundle.putString("state",state);
      bundle.putString("country",country);
      resultReceiver.send(resultcode,bundle);
    }else {
      Toast.makeText(this,"Result receiver is null",Toast.LENGTH_SHORT).show();
    }
  }
}
